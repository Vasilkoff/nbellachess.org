import React, { useState } from 'react';
import Link from 'next/link';


export default function NoAccess() {
  return (
  	<>
	<p>This page is available only for authorized users.</p>
          
    <p> Please
      <Link href="/users/login.html">
        <a> Login</a>
      </Link>
      &nbsp;or&nbsp;
      <Link href="/users/register.html">
        <a>Register</a>
      </Link>
    </p>
    </>
  )
}