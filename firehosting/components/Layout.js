import React, { useState } from 'react';
import Header from '../components/Header'
import Footer from '../components/Footer'

export default function Layout({ children }) {
  return (
    <div className="is-boxed has-animations lights-off">
    	<Header/>
      		<div className="body-wrap boxed-container">{children}</div>
      	<Footer/>
    </div>
  )
}