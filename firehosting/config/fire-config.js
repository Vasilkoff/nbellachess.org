//to use firebase app
//import firebase from 'firebase/app'; //older version
import firebase from 'firebase/compat/app'; //v9

// //to use auth
// import 'firebase/auth'; //older version
import 'firebase/compat/auth'; //v9

// //to use firestore
// import 'firebase/firestore'; //Older Version
import 'firebase/compat/firestore'; //v9

const firebaseConfig = {
  apiKey: "AIzaSyDFiFCovx4bLdNXi4z2E1zfzbP1ti9db-U",
  authDomain: "bellachessorg.firebaseapp.com",
  projectId: "bellachessorg",
  storageBucket: "bellachessorg.appspot.com",
  messagingSenderId: "148084878949",
  appId: "1:148084878949:web:6bf3d3bd2794e4b1b5608c",
  measurementId: "G-MYPBV47HNV"
};

try {
  firebase.initializeApp(firebaseConfig);
} catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)}
}

const fire = firebase;
export default fire;