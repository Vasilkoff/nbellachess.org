import Head from 'next/head'


export default function Home() {
  return (
    <>
	<Head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png"/>
		<link rel="manifest" href="./site.webmanifest"/>
		<link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5"/>
		<meta name="msapplication-TileColor" content="#da532c"/>
		<meta name="theme-color" content="#ffffff"/> 
		<link rel="icon" href="./favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<title>BellaChess Tokens play and earn</title>

		<meta name="description" content="Withdraw your earned BellaChess crypto tokens ($BLCH)"/>

		<meta property="og:url" content="https://bellachess.org/"/>
		<meta property="og:type" content="website"/>
		<meta property="og:title" content="BellaChess Tokens play and earn"/>
		<meta property="og:description" content="Withdraw your earned BellaChess crypto tokens"/>
		<meta property="og:image" content="https://bellachess.org/bella-screenshot.png"/>

		<meta name="twitter:card" content="summary_large_image"/>
		<meta property="twitter:domain" content="Withdraw your earned BellaChess crypto tokens"/>
		<meta property="twitter:url" content="https://bellachess.org/"/>
		<meta name="twitter:title" content="BellaChess Tokens play and earn"/>
		<meta name="twitter:description" content="Withdraw your earned BellaChess crypto tokens"/>
		<meta name="twitter:image" content="https://bellachess.org/bella-screenshot.png"/>

	</Head>
	<main>
            <section class="hero">
                <div class="container">
                    <div class="hero-inner">
						<div class="hero-copy">
	                        <h1 class="hero-title mt-0">BellaChess - play and earn $BLCH crypto tokens!</h1>
	                        <p class="hero-paragraph">Download the game from PlayMarket and come back here to withdraw your token to your Binance wallet</p>
	                        <div class="hero-cta">
								<a href='/' target="_blank"><img class="GooglePlayBadge" src="dist/images/GooglePlayBadge.png" alt="Logo BellaChess"/></a>
								<a class="button button-primary" href="/users/login.html">Login</a>
							</div>
						</div>
                    </div>
                </div>
            </section>

            <section class="features section">
                <div class="container">
					<div class="features-inner section-inner">
						<div class="features-header text-center">
							<div class="container-sm">
								<h2 class="section-title mt-0">The Game</h2>
	                            <p class="section-paragraph">BellaChess is a classic Chess game to play against a computer and another human opponent online. 
	                            When you win you earn a BellaChess crypto token which is minted on Binance Blockchain. Please, learn more details in our <a href="/faq.html">FAQ</a> page</p>
								<div class="features-image">
                                    <img class="features-illustration" src="dist/images/chessBoard.png" alt="Chess board" width="320"/>
								</div>
							</div>
                        </div>
                        <div class="features-wrap">
                            <div class="feature is-revealing">
                                <div class="feature-inner">
                                    <div class="feature-icon">
										<img class="image" src="/bellachess-logo.png" alt="BellaChess"/>
                                    </div>
									<div class="feature-content">
                                    	<h3 class="feature-title mt-0">Download the game</h3>
                                    	<p class="text-sm mb-0">Download the app to manage your projects, keep track of the progress and complete tasks without procastinating. Stay on track and complete on time!</p>
									</div>
								</div>
                            </div>
							<div class="feature is-revealing">
                                <div class="feature-inner">
                                    <div class="feature-icon">
										<img class="image" src="/bellachess-logo.png" alt="BellaChess"/>
                                    </div>
									<div class="feature-content">
                                    	<h3 class="feature-title mt-0">Create your account</h3>
                                    	<p class="text-sm mb-0">Account in the game allows you to withdraw the token you earned in the game to be transfered to your Binance Wallet.</p>
									</div>
								</div>
                            </div>
							<div class="feature is-revealing">
                                <div class="feature-inner">
                                    <div class="feature-icon">
										<img class="image" src="/bellachess-logo.png" alt="BellaChess"/>
                                    </div>
									<div class="feature-content">
                                    	<h3 class="feature-title mt-0">Trade BellaChess Token</h3>
                                    	<p class="text-sm mb-0">BellaChess Token - BCHL is a Binance BEP20 Token available to buy and sell on PancakeSwap</p>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

			<section class="cta section">
                <div class="container-sm">
                    <div class="cta-inner section-inner">
                        <div class="cta-header text-center">
                            <h2 class="section-title mt-0">Boosters and NFTs</h2>
                            <p class="section-paragraph">List of BellaChess NFTs is coming soon. To be informed about news of the project, please, subscribe to our email updates.</p>
							<div class="cta-cta">
								<a class="button button-primary" href="#">Subscribe</a>
							</div>
					    </div>
                    </div>
                </div>
            </section>
        </main>

    </>
  )
}
