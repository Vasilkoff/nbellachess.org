import Head from 'next/head'


export default function PP() {
  return (
    <>
	<Head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png"/>
		<link rel="manifest" href="./site.webmanifest"/>
		<link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5"/>
		<meta name="msapplication-TileColor" content="#da532c"/>
		<meta name="theme-color" content="#ffffff"/> 
		<link rel="icon" href="./favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta
		  name="description"
		  content="Withdraw your earned BellaChess crypto token here"
		/>
		<title>BellaChess - Privacy Policy</title>
	</Head>

    <main>
    	<section class="statictext">
        <div class="container">
    			<h1>Privacy Policy</h1>


<p>
	Welcome to <strong>BellaChess</strong>!
</p>
<p>
	<a href="https://BellaChess.org" target="_blank" rel="noopener">BellaChess.org</a> website 
	and Android Mobile application BellaChess are owned by <strong>NFTRX Ltd</strong> ("NFTTRX", "Company", "we", "us" and/or "our"). 
</p>
<p>	
	NFTTRX provides sevices related to development, hosting, minting, trading and management of Crypto Tokens and NFTs ("Services"). 
	In order to provide our Site and Services (collectively, "Platform"), we collect personal data from our customers ("Customers"). 
	Throughout this Privacy Policy, "personal information" or "personal data" refers to any information that is unique to an individual, 
	such as name, address, email address, phone number, IP address and other information that can reasonably identify an individual.
</p>
<p>
	This Privacy Policy, along with our <a href="#cookie-policy">Cookie Policy</a>, 
	covers how we collect, handle and disclose personal data on our Platform, including any affiliated websites, software or platforms owned 
	or operated by Vercel on which this Privacy Policy appears. Even though we store End Users' personal data, it does not cover how or 
	why our Customers may collect, handle and disclose their End Users' personal data when they visit or use their websites or platforms.
</p>
<p>
	<strong>If you are located in the European Economic Area ("EEA") or the United Kingdom ("UK")</strong>, 
	this entire Privacy Policy applies to you. However, please see the Section titled  <a href="#eea">"Additional Information for Users in the EEA and the UK."</a>, 
	which will provide more detail information about which rights you have regarding the processing of your personal data.
</p>
<p>
	We recommend that you read this Privacy Policy and our Cookie Policy carefully as both provide important information about your personal information and your rights under the law.
</p>
<p>
	If you have any questions, comments, or concerns regarding this Privacy Policy, our Cookie Policy and/or our data practices, or would like to exercise your rights, 
	do not hesitate to contact us at  <a href="mailto:Privacy@BellaChess.org">Privacy@BellaChess.org</a>  or see our information below.
</p>

<div id="us">
	<a href="#us">
		<h2>Who we are and How to Contact us</h2>
	</a>
	<p>
		NFTTRX is a limited company with the following contact information:
	</p>
	<p>
		<address>
			NFTTRX Ltd<br/>
			20-22 WENLOCK ROAD<br/>
			London, England, N1 7GU<br/>
			<a href="mailto:Privacy@BellaChess.org">Privacy@BellaChess.org</a>
		</address>
	</p>
</div>
<div id="who">
	<a href="#who">
		<h2>To Whom does this Policy apply</h2>
	</a>
	<p>
		This Privacy Policy applies to:
		<ul>
			<li>
				<strong>Site Visitors: </strong>visitors to our Site.
			</li>
			<li>
				<strong>Mobile application users: </strong> those who downloaded and use the app published in the Play Market.
			</li>
		</ul>
	</p>		
</div>




    		</div>
    	</section>
    </main>
    </>
    )
}
