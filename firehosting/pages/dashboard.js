import { useState, useEffect } from 'react';
import Head from 'next/head';
import fire from '../config/fire-config';
import { useRouter } from 'next/router'
import NoAccess from '../components/NoAccess';



const Dashboard = () => {
  const router = useRouter();
  const [blogs, setBlogs] = useState([]);
  const [notification, setNotification] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [waddress, setWAddress] = useState('');
  const [tamount, setTAmount] = useState(0);
  const [userObj, setUser] = useState({});
  const [wait, setWait] = useState(true);
  const [balance, setBalance] = useState(0);


fire.auth().onAuthStateChanged((user) => {
  setWait(false);
  if (user) {
    setLoggedIn(true)
    setUser(user)
  } else {
    console.log('Logged out')
  }
})

const handleLogout = () => {
  fire.auth()
    .signOut()
    .then(() => {
      setNotification('Logged out')
      setTimeout(() => {
        setNotification('')
        setLoggedIn(false)
        setUser({})
        router.push("/");
      }, 2000)
    });
}

const handleWithdraw = (e) => {
  e.preventDefault();
  setNotification('Function not yet implemented')
  setTimeout(() => {
    setNotification('');
  }, 2000)
}

return (
<>
<section class="login">
<div class="container">

{!loggedIn 
?
        <div class="whiteBox">

          <div class="notification"> {notification} </div>
          {!wait
          ?
           <NoAccess />
          :
           <p class="text-center">Loading...</p>
          }
        </div>

:
        <div class="whitePanel">
          <p><b>Welcome</b> <i>{userObj.email}</i> &nbsp;|&nbsp; <a onClick={handleLogout}>Logout</a></p>
          <hr/>
          <p>Available Balance:  {balance}</p>
          <i>{notification} &nbsp;</i>
          <form onSubmit={handleWithdraw}>
            <label>Tron wallet address</label>
            <input type="text" value={waddress} onChange={({target}) => setWAddress(target.value)} />
            <br />
            <label>Amount of BLCH</label>
            <input type="number" value={tamount} onChange={({target}) => setTAmount(target.value)} />
            <br />
            <button type="submit" class="button button-primary">Withdraw</button>
          </form>
        </div>
}

</div>
</section>
</>
  )
}

export default Dashboard;