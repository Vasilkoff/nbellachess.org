import Head from 'next/head'


export default function TnC() {
  return (
    <>
	<Head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png"/>
		<link rel="manifest" href="./site.webmanifest"/>
		<link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5"/>
		<meta name="msapplication-TileColor" content="#da532c"/>
		<meta name="theme-color" content="#ffffff"/> 
		<link rel="icon" href="./favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta
		  name="description"
		  content="Withdraw your earned BellaChess crypto token here"
		/>
		<title>BellaChess - Terms and Conditions</title>
	</Head>

    <main>
      <section class="statictext">
        <div class="container">
    			<h1>Terms and Conditions</h1>


<p>
	Welcome to <strong>BellaChess</strong>!
</p>
<p>These terms and conditions outline the rules and regulations for the use of BellaChess website, located at <a href="https://BellaChess.org" target="_blank" rel="noreferrer noopener">https://BellaChess.org</a>.</p>
<p>By accessing this website we assume you accept these terms and conditions. Do not continue to use BellaChess.org if you do not agree to take all of the terms and conditions stated on this page.</p>
<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: “Client”, “You” and “Your” refers to you, the person log on this website and compliant to the Company’s terms and conditions. “The Company”, “Ourselves”, “We”, “Our” and “Us”, refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>

<h2><strong>Cookies</strong></h2>
<p>We employ the use of cookies. By accessing BellaChess.org, you agreed to use cookies in agreement with the BellaChess.org’s Privacy Policy.</p>
<p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>

<h2><strong>License</strong></h2>
<p>Unless otherwise stated, BellaChess.org and/or its licensors own the intellectual property rights for all material on BellaChess.org. All intellectual property rights are reserved. You may access this from BellaChess.org for your own personal use subjected to restrictions set in these terms and conditions.</p>
<p>You must not:</p>
<ul><li>Republish material from BellaChess.org</li><li>Sell, rent or sub-license material from BellaChess.org</li><li>Reproduce, duplicate or copy material from BellaChess.org</li><li>Redistribute content from BellaChess.org</li></ul>
<p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. BellaChess.org does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of BellaChess.org,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, BellaChess.org shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>
<p>BellaChess.org reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>
<p>You warrant and represent that:</p>
<ul><li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li><li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li><li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li><li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li></ul>
<p>You hereby grant BellaChess.org a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>

<h2><strong>Your Privacy</strong></h2>
<p>Please, read our <a href="/privacy-policy.hrml" target="_blank">Privacy Policy</a></p>


<h2><strong>Removal of content from our website</strong></h2>
<p>If you find any content on our Website that is offensive or inappropriate for any reason, you are free to contact and inform us any moment. We will consider requests to remove that content but we are not obligated to or so or to respond to you directly.</p>
<p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>

<h2><strong>Disclaimer</strong></h2>
<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>
<ul><li>limit or exclude our or your liability for death or personal injury;</li><li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li><li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li><li>exclude any of our or your liabilities that may not be excluded under applicable law.</li></ul>
<p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>
<p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>



    		</div>
    	</section>
    </main>
    </>
    )
}