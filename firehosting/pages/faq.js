import Head from 'next/head'


export default function FAQ() {
  return (
    <>
	<Head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="180x180" href="./apple-touch-icon.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png"/>
		<link rel="manifest" href="./site.webmanifest"/>
		<link rel="mask-icon" href="./safari-pinned-tab.svg" color="#5bbad5"/>
		<meta name="msapplication-TileColor" content="#da532c"/>
		<meta name="theme-color" content="#ffffff"/> 
		<link rel="icon" href="./favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta
		  name="description"
		  content="Withdraw your earned BellaChess crypto token here"
		/>
		<title>BellaChess - FAQ</title>
	</Head>

    <main>
    	<section class="statictext">
        <div class="container">
    			<h1>Frequently Asked Questions</h1>

<h2>What is BellaChess?</h2>
<p>
	BellaChess is a classic Chess game to play against a computer and another human opponent online. 
	However, along with this, BellaChess uses blockchain technologies, DeFi and NFT. 
	When you win you earn a BellaChess crypto token which is minted on Binance Smart Chain Blockchain.
	The key feature of the project is that you can play your favorite game and earn good money at the same time.
</p>

<h2>What is $BLCH token?</h2>
<p>
	BLCH is BellaChess's gaming token. It is used for in-game rewards and enables smart contracts to work on the blockchain.
	The BellaChess token can be bought and sold on Binance DEX. The token standard is BEP-20. 
	The token is created on the basis of Binance Smart Chain.
</p>


<h2>How can I play the game?</h2>
<p>
	Please, search for BellaChess in PlayMarket on your Android phone or use the download link on the front page of BellaChess.org
</p>

<h2>What blockchain is used for the game assets?</h2>
<p>The game token is powered by Binance Smart Chain</p>

<h2>How to trade $BLCH token?</h2>
<p>Currently, the BellaChess token can be bought and sold on PancakeSwap DEX and any others supporting BEP-20 standard DEXs. In the future the number of Exchanges will be extended.</p>

    		</div>
    	</section>
    </main>
    </>
    )
}