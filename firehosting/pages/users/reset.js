import { useState } from 'react';
import fire from '../../config/fire-config';
import { useRouter } from 'next/router'

const Reset = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [notify, setNotification] = useState('');
  const router = useRouter();

  const handleReset = (e) => {
    e.preventDefault();

    fire.auth()
      .signInWithEmailAndPassword(username, password)
      .then((userCredential) => {
        // Signed in 
        setNotification('Successfully logged in!')
        const user = userCredential.user;
        console.log(user);
        setTimeout(() => {
          setNotification('');
          setUsername('');
          setPassword('');
          router.push("/dashboard.html");
        }, 2500)
      })
      .catch((err) => {

        console.log(err.code, err.message)
        setNotification("Error"+err.message.split('Firebase').join(''))

        setTimeout(() => {
          setNotification('')
        }, 15000)
      })

    
  }

  return (
      <main>
        <section class="login">
          <div class="container">
          <div class="whiteBox">
              <p>Forgot password form</p>
              <h1>Reset your password</h1>
              <p>{notify}</p>
              <form onSubmit={handleReset}>
                <label>Email</label>
                <input type="text" value={username} onChange={({target}) => setUsername(target.value)} />
                <br />
                <label>Password</label>
                <input type="password" value={password} onChange={({target}) => setPassword(target.value)} />
                <p class="text-center"><a href="/users/register.html">Register</a>&nbsp;|&nbsp;<a href="/users/login.html">Login</a></p>
                <button type="submit" class="button button-primary">Login</button>
              </form>
            </div>
          </div>
        </section>
      </main>
  )

}

export default Reset
