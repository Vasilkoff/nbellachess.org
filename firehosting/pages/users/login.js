import { useState } from 'react';
import fire from '../../config/fire-config';
import { useRouter } from 'next/router'

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [notify, setNotification] = useState('');
  const router = useRouter();


  fire.auth().onAuthStateChanged((user) => {
    if (user) {
      router.push("/dashboard.html");
    }
  })

  const handleLogin = (e) => {
    e.preventDefault();

    fire.auth()
      .signInWithEmailAndPassword(username, password)
      .then((userCredential) => {
        // Signed in 
        setNotification('Successfully logged in!')
        const user = userCredential.user;
        console.log(user);
        setTimeout(() => {
          setUsername('');
          setPassword('');
          router.push("/dashboard.html");
        }, 250)
      })
      .catch((err) => {
        console.log(err.code, err.message)
        setNotification("Error"+err.message.split('Firebase').join(''))
      })
  }

  const handleOK = (e) => {
    e.preventDefault();
    setNotification('');
  }

  return (
      <main>
        <section class="login">
          <div class="container">
          <div class="whiteBox">
              <p>Welcome back</p>
              <h1>Login to your account</h1>
              <p>{notify}</p>
              {!notify
                ?
                <form onSubmit={handleLogin}>
                  <label>Email</label>
                  <input type="text" value={username} onChange={({target}) => setUsername(target.value)} />
                  <br />
                  <label>Password</label>
                  <input type="password" value={password} onChange={({target}) => setPassword(target.value)} />
                  <p class="text-center"><a href="/users/register.html">Register</a>&nbsp;|&nbsp;<a href="/users/reset.html">Forgot passwrod</a></p>
                  <button type="submit" class="button button-primary">Login</button>
                </form>
                :
                <button class="button button-primary" onClick={handleOK}>OK</button>
              }
            </div>
          </div>
        </section>
      </main>
  )

}

export default Login
