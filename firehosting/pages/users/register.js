import { useState } from 'react'; 
import fire from '../../config/fire-config';
import { useRouter } from 'next/router'

const Register = () => {

  const router = useRouter();

  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passConf, setPassConf] = useState('');

  const [notify, setNotification] = useState('');


  fire.auth().onAuthStateChanged((user) => {
    if (user) {
      router.push("/dashboard.html");
    }
  })

  const handleRegister = (e) => {
    e.preventDefault();
    setNotification('');

    if (password !== passConf) {
      setNotification('Error: Password and password confirmation does not match')

      setTimeout(() => {
        setNotification('')
      }, 15000)

      setPassword('');
      setPassConf('');
      return null;

    } else {
      fire.auth()
      .createUserWithEmailAndPassword(userName, password)
      .then((userCredential) => {
        setNotification('Successfully created a new user account!')
        // Signed in 
        const user = userCredential.user;
        console.log(user);

        setTimeout(() => {
          router.push("/dashboard.html");
          setNotification('');
          setUsername('');
          setPassword('');
        }, 2500)

      })
      .catch((err) => {
        console.log(err.code, err.message)
      });
    }
    
  }

  return (


    <main>
        <section class="login">
          <div class="container">
          <div class="whiteBox">
              <p>Create a new user</p>
              <h1>Register an account</h1>
              {notify}
              <form onSubmit={handleRegister}>
                <label>Email</label>
                <input type="text" value={userName} onChange={({target}) => setUsername(target.value)} />
                <br />
                <label>Password</label>
                <input type="password" value={password} onChange={({target}) => setPassword(target.value)} /> 
                <br />
                <label>Password confirmation</label>
                <input type="password" value={password} value={passConf} onChange={({target}) => setPassConf(target.value)} /> 
                <p class="text-center"><a href="/users/login.html">Login</a>&nbsp;|&nbsp;<a href="/users/reset.html">Forgot passwrod</a></p>
                <button type="submit" class="button button-primary">Register</button>
              </form>
            </div>
          </div>
        </section>
      </main>

  )
}

export default Register